﻿using Microsoft.Extensions.Logging;
using Slack.Webhooks;
using System;
using System.Collections.Generic;

namespace ExcelGenerator
{
    class Logger
    {
        public List<string> logSoFar = new List<string>();
        public System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
        private readonly ILogger _log;
        private readonly string _jobId;
        private readonly bool _isDebug;

        public Logger(ILogger logIn, string jobId, bool isDebug)
        {
            _log = logIn;
            _jobId = jobId;
            _isDebug = isDebug;
            stopwatch.Start();
        }

        public void Log(string message)
        {
            string logText = Math.Round(stopwatch.ElapsedMilliseconds / 1000.0, 2) + " secs: " + message;
            string cutDownJobId = (_jobId.Length > 10 ? "..." + _jobId.Substring(_jobId.Length - 10) : _jobId);
            _log.Log(Microsoft.Extensions.Logging.LogLevel.Information, "[" + cutDownJobId + "] " + logText);
            logSoFar.Add(logText);
        }
        public void SaySlack(bool isError, string msg)
        {
            var slackClient = new SlackClient("https://hooks.slack.com/services/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");
            var slackMessage = new SlackMessage
            {
                Channel = "#app_status",
                Username = "Excel Plugin",
                Text = msg
            };
            if (!_isDebug)
                slackClient.Post(slackMessage);
            if (isError)
            {
                slackMessage.Channel = "#app_errors";
                if (!_isDebug)
                    slackClient.Post(slackMessage);
            }
        }

    }
}
