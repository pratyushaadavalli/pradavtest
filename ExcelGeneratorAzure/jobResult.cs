﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExcelGeneratorAzure
{
    public class JobResult
    {
        public string JobId = "";
        public string ResultURL = "";
        public string Status = "";
        public string ErrorMessage = "";
        public string StackTrace = "";
    }
}
