using System;
using System.IO;
using Microsoft.Azure.WebJobs;
using ExcelGenerator;
using System.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Threading.Tasks;
using System.Net;
using DocumentFormat.OpenXml.Extensions;
using Slack.Webhooks;
using static ExcelGenerator.V8cExcelWriter;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;

namespace ExcelGeneratorAzure
{
    public static class GetWorkbook
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        private static async Task SaveLogsAndReportStatusAsync(JobResult jRes, CloudBlockBlob logFileBlob, Logger log)
        {
            string logUri;
            if (logFileBlob == null)
                logUri = "Failed to save log file";
            else
                logUri = logFileBlob.Uri.AbsoluteUri;
            string msg = "ExcelGenerator result for job " + jRes.JobId + ": " + jRes.Status + " in " + (log.stopwatch.ElapsedMilliseconds / 1000).ToString() 
                + " seconds. Log file: <" + logUri + ">";
            if (jRes.Status != "Success")
                msg += ". Error message: " + jRes.ErrorMessage;
            log.Log("Finished. Result: " + JsonConvert.SerializeObject(jRes));
            try
            {
                // Save the log file
                await logFileBlob.UploadTextAsync(string.Join("\n", log.logSoFar));
            }
            catch { }
            log.SaySlack((jRes.Status != "Success"), msg);
            System.GC.Collect(); // to avoid the function sitting with memory usage in Azure when it doesn't really have any
        }

        [FunctionName("GetInspectWorkbook")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static async Task GetInspectWorkbook([BlobTrigger("transfer-inspect/{uuid}.zip", Connection = "valid8excel_STORAGE")]Stream sZip, string uuid, ILogger logIn, ExecutionContext executionContext)
        {
            JobResult jRes = null;
            string cBack = null;
            string runningLocally = Environment.GetEnvironmentVariable("localrun");
            string mName = "GetInspectWorkbook";
            bool isCanary = (uuid.StartsWith("canary-automation-"));
            Logger log = new Logger(logIn, uuid, (runningLocally != null) | isCanary);
            ToLog LogMethod = log.Log;
            LogMethod("Detected " + mName + " blob change - " + uuid);
            LogMethod("System environment: " + (System.Environment.Is64BitProcess ? "64-bit" : "32-bit"));
            string path = executionContext.FunctionAppDirectory;
            CloudStorageAccount ioStorageAccount = null;
            CloudBlockBlob logFileBlob = null;

            try
            {
                string ioConnectionString = Environment.GetEnvironmentVariable("valid8excel_STORAGE");
                CloudStorageAccount.TryParse(ioConnectionString, out ioStorageAccount);
                log.SaySlack(false, "ExcelGenerator " + mName + " starting on blob " + ioStorageAccount.Credentials.AccountName + @"\" + uuid);

                CloudBlobClient ioBlobClient = ioStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer dContainer = ioBlobClient.GetContainerReference("transfer-inspect");
                CloudBlockBlob xlsxBlob = dContainer.GetBlockBlobReference(uuid + ".xlsx");
                logFileBlob = dContainer.GetBlockBlobReference(uuid + ".log");

                ReturnStructure rs = SpreadsheetBuilder.BuildInspectWorkbook(LogMethod, sZip, path + @"\templates\inspect.xlsx");

                LogMethod("Saving to blob");
                // If we specified a custom callback URL, use that
                if (rs.callBackUrl != null)
                    cBack = rs.callBackUrl;
                await xlsxBlob.UploadFromFileAsync(rs.spreadsheetFile);

                // Clean out old files
                File.Delete(rs.spreadsheetFile);
                await SpreadsheetBuilder.CleanOldBlobsAsync(LogMethod, dContainer);
                // Create the secondary result structure
                jRes = new JobResult { JobId = uuid, ResultURL = xlsxBlob.Uri.AbsoluteUri, Status = "Success" };
            }
            catch (Exception e)
            {
                // The thing failed for some reason - make an error result instead
                jRes = new JobResult { JobId = uuid, ErrorMessage = e.Message, StackTrace = e.StackTrace, Status = "Fail" };
            }

            try
            {
                // Pass back the result
                using var client = new HttpClient();
                // Only bother calling the callback if we're running on live or staging sites, not locally
                if (!string.IsNullOrEmpty(cBack))
                {
                    LogMethod("Calling back to: " + cBack);
                    HttpResponseMessage response = await client.PostAsync(
                        cBack,
                         new StringContent(JsonConvert.SerializeObject(jRes), Encoding.UTF8, "application/json"));
                    LogMethod("POST result: " + response.StatusCode.ToString());
                }
                else
                {
                    LogMethod("Running locally so not bothering with callback");
                }
            }
            catch (Exception e)
            {
                // The callback failed - make an error result instead
                jRes = new JobResult { JobId = uuid, ErrorMessage = e.Message, StackTrace = e.StackTrace, Status = "Fail" };
            }
            await SaveLogsAndReportStatusAsync(jRes, logFileBlob, log);
        }

        [FunctionName("GetVerifyWorkbook")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static async Task GetVerifyWorkbook([BlobTrigger("transfer-verify/{uuid}.zip", Connection = "valid8excel_STORAGE")]Stream sZip, string uuid, ILogger logIn, ExecutionContext executionContext)
        {
            JobResult jRes = null;
            string cBack = null;
            string runningLocally = Environment.GetEnvironmentVariable("localrun");
            string mName = "GetVerifyWorkbook";
            bool isCanary = (uuid.StartsWith("canary-automation-"));
            Logger log = new Logger(logIn, uuid, (runningLocally != null) | isCanary);
            ToLog LogMethod = log.Log;
            LogMethod("Detected " + mName + " blob change - " + uuid);
            LogMethod("System environment: " + (System.Environment.Is64BitProcess ? "64-bit" : "32-bit"));
            string path = executionContext.FunctionAppDirectory;
            CloudStorageAccount ioStorageAccount = null;
            CloudBlockBlob logFileBlob = null;
            try
            {
                string ioConnectionString = Environment.GetEnvironmentVariable("valid8excel_STORAGE");
                CloudStorageAccount.TryParse(ioConnectionString, out ioStorageAccount);
                log.SaySlack(false, "ExcelGenerator " + mName + " starting on blob " + ioStorageAccount.Credentials.AccountName + @"\" + uuid);

                CloudBlobClient ioBlobClient = ioStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer dContainer = ioBlobClient.GetContainerReference("transfer-verify");
                CloudBlockBlob xlsxBlob = dContainer.GetBlockBlobReference(uuid + ".xlsx");
                logFileBlob = dContainer.GetBlockBlobReference(uuid + ".log");

                ReturnStructure rs = SpreadsheetBuilder.BuildVerifyWorkbook(LogMethod, sZip, path + @"\templates\verify.xlsx", uuid);
                LogMethod("Saving to blob");
                // If we specified a custom callback URL, use that
                if (rs.callBackUrl != null)
                    cBack = rs.callBackUrl;
                await xlsxBlob.UploadFromFileAsync(rs.spreadsheetFile);

                // Clean out old files
                File.Delete(rs.spreadsheetFile);
                await SpreadsheetBuilder.CleanOldBlobsAsync(LogMethod, dContainer);
                // Create the secondary result structure
                jRes = new JobResult { JobId = uuid, ResultURL = xlsxBlob.Uri.AbsoluteUri, Status = "Success" };
            }
            catch (Exception e)
            {
                // The thing failed for some reason - make an error result instead
                jRes = new JobResult { JobId = uuid, ErrorMessage = e.Message, StackTrace = e.StackTrace, Status = "Fail" };
            }

            try
            {
                // Pass back the result
                using var client = new HttpClient();
                // Only bother calling the callback if we're running on live or staging sites, not locally
                if (!string.IsNullOrEmpty(cBack))
                {
                    LogMethod("Calling back to: " + cBack);
                    HttpResponseMessage response = await client.PostAsync(
                        cBack,
                         new StringContent(JsonConvert.SerializeObject(jRes), Encoding.UTF8, "application/json"));
                    LogMethod("POST result: " + response.StatusCode.ToString());
                }
                else
                {
                    LogMethod("Running locally so not bothering with callback");
                }
            }
            catch (Exception e)
            {
                // The callback failed - make an error result instead
                jRes = new JobResult { JobId = uuid, ErrorMessage = e.Message, StackTrace = e.StackTrace, Status = "Fail" };
            }
            await SaveLogsAndReportStatusAsync(jRes, logFileBlob, log);
        }
    }
}
