﻿using DocumentFormat.OpenXml.Extensions;
using ExcelGenerator;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace TryingCoreConsole
{
    class Program
    {
        static readonly Stopwatch stopwatch = new Stopwatch();

        public static void LogMethod(string message)
        {
            System.Console.WriteLine(Math.Round(stopwatch.ElapsedMilliseconds / 1000.0, 2) + " secs: " + message);
        }

        private static ReturnStructure RunInspect()
        {
            // Trying to pass stream, like Azure does
            MemoryStream zipFile = new MemoryStream(File.ReadAllBytes(@"..\..\..\..\sampleData\inspect\sample.zip"));

            return SpreadsheetBuilder.BuildInspectWorkbook(
                LogMethod,
                zipFile,
                @"..\..\..\..\ExcelGeneratorAzure\templates\inspect.xlsx");
        }

        private static ReturnStructure RunVerify()
        {
            // Trying to pass stream, like Azure does
            //MemoryStream zipFile = new MemoryStream(File.ReadAllBytes(@"..\..\..\..\sampleData\verify\large.zip"));
            //MemoryStream zipFile = new MemoryStream(File.ReadAllBytes(@"..\..\..\..\sampleData\verify\medium.zip"));
            MemoryStream zipFile = new MemoryStream(File.ReadAllBytes(@"..\..\..\..\sampleData\verify\sample.zip"));

            return SpreadsheetBuilder.BuildVerifyWorkbook(
                LogMethod,
                zipFile,
                @"..\..\..\..\ExcelGeneratorAzure\templates\verify.xlsx", "noID");
        }

        static void Main()
        {
            Console.WriteLine("You can run:");
            Console.WriteLine(" (V)erify/Audit");
            Console.WriteLine(" (I)nspect");
            char k;
            do
            {
                k = Console.ReadKey().KeyChar.ToString().ToLower()[0];
            } while ((k != 'v') && (k != 'i'));
            stopwatch.Start();
            ReturnStructure rs = null;
            if (k == 'v')
                rs = RunVerify();
            if (k == 'i')
                rs = RunInspect();
            //LogMethod("Memory usage: " + (GC.GetTotalMemory(true) / 1024 / 1024).ToString() + "mb");
            Process.Start(@"cmd.exe", "/c " + rs.spreadsheetFile);

            //System.GC.Collect(); // breakpoint on next line if you want to track memory
            LogMethod("Finished");
            //Console.ReadKey();
        }
    }
}
 
