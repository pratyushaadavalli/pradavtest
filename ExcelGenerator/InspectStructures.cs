﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelGenerator
{
    public class InsBalancesStructure
    {
        public List<InsBalancesItem> data = null;
    }

    public class InsBalancesItem
    {
        public string id;
        public string name;
        public string number;
        public string sourceType;
        public string description;
        public string bank;
        public string type;
        public string entity;
        public Balance[] balances;
    }

    public class Balance
    {
        public float? balance;
        public string date;
    }

    public class InsAccountsStructure
    {
        public Dictionary<string, InsAccountItem> data = null;
        // public List<account> data = null;
    }

    public class InsAccountItem
    {
        //"id": "754c9f55-b5a6-4168-9509-cdeaeb7db3a9",
        //"name": "Capital One CC 1",
        //"number": "4154179706894464",
        //"bank": "Capital One",
        //"startDate": null,
        //"endDate": null,
        //"description": null

        public string id = "";
        public string name = "";
        public string number = "";
        public string bank = "";
        public Nullable<DateTime> startDate = null;
        public Nullable<DateTime> endDate = null;
        public string description = "";
        public string accountType = "";
        public Nullable<double> balanceBeginning = null;
        public Nullable<double> balanceEnding = null;
        public Nullable<DateTime> balanceStartDate = null;
        public Nullable<DateTime> balanceEndDate = null;
        public Nullable<double> inflows = null;
        public Nullable<double> outflows = null;
        public Nullable<double> netActivity = null;
        public string entityName = "";
    }

    public class InsMetaStructure
    {
        public InsMetaItem metadata = null;
    }

    public class InsMetaItem
    {
        public string emailAddress = "";
        public string displayName = "";
        public bool deleteMe = false;
        public string callback;
        public string type = "";
        public InsFilterStructure filterState = null;
        public string locale = "en_US";
        public string meVersion = "";
        public string v8Version = "";
    }

    public class InsFilterStructure
    {
        public Nullable<DateTime> startDate = null;
        public Nullable<DateTime> endDate = null;
        public Nullable<double> transactionMin = null;
        public Nullable<double> transactionMax = null;
        public List<string> accounts;
        public List<string> category;
        public List<string> tags;
        public string keywords = "";
    }


    public class ExportStructure
    {
        public List<Transaction> data = null;
    }

    public class Transaction
    {
        public string accountId = "";
        public string description = "";
        public string name = "";
        public string memo = "";
        public List<string> tags = new List<string>();
        public string to = "";
        public string from = "";
        public Nullable<DateTime> date = null;
        public Nullable<double> amount = null;
        public Nullable<double> balance = null;
        public List<Dictionary<string, string>> visCategories = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> customFields = new List<Dictionary<string, string>>();
        public string checkNo = "";
        public string transactionKind = "";
        public string id = "";
        // Need to add tags
        public string matchId = "";
        public string currency = "";
        public string accountPeriodId = "";
        public string filename = "";
        public int page = 0;
    }
}
