﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace ExcelGenerator
{
    internal class CellReference
    {
        public string localReference;
        public string localReferenceNoDollars;
        public string sheetName;

        public CellReference(string sheetName, string cellRef)
        {
            this.sheetName = sheetName;
            this.localReference = cellRef;
            this.localReferenceNoDollars = cellRef.Replace("$","");
        }

        internal static CellReference FromDefinedName(DefinedName dn)
        {
            return new CellReference(dn.InnerText.Split("!".ToCharArray())[0], dn.InnerText.Split("!".ToCharArray())[1]);
        }
    }
}