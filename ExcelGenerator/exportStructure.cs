﻿using System;
using System.Collections.Generic;

namespace ExcelGenerator
{
    public class JobResult
    {
        public string JobId = "";
        public string ResultURL = "";
        public string Status = "";
        public string ErrorMessage = "";
        public string StackTrace = "";
    }
}