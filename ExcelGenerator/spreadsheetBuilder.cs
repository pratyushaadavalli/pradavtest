﻿using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Azure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using static ExcelGenerator.V8cExcelWriter;

namespace ExcelGenerator
{
    public static class SpreadsheetBuilder
    {
        public static async System.Threading.Tasks.Task CleanOldBlobsAsync(ToLog logMethod, CloudBlobContainer cbc)
        {
            int DAYS_TO_KEEP_OUTPUTS = 5;

            logMethod("Removing input files more than " + DAYS_TO_KEEP_OUTPUTS + " day(s) old");
            // This uses paging but I don't really care as the max is 5000
            var blobSegment = await cbc.ListBlobsSegmentedAsync("", true, BlobListingDetails.All, null, null, null, null);
            //.OfType<CloudBlockBlob>().Where(b => (DateTime.UtcNow.AddDays(-DAYS_TO_KEEP_OUTPUTS) > b.Properties.LastModified.Value.DateTime)).ToList();

            foreach (var item in blobSegment.Results)
            {
                if (item is CloudBlockBlob thisBlob)
                {
                    var blob = item as CloudBlockBlob;
                    var dt = blob.Properties.LastModified;
                    if (DateTime.UtcNow.AddDays(-DAYS_TO_KEEP_OUTPUTS) > dt.Value.DateTime)
                    {
                        logMethod($"Deleting old blob: {item.Uri}, modified {dt}");
                        await thisBlob.DeleteIfExistsAsync();
                    }
                }
            }
        }

        public static ReturnStructure BuildVerifyWorkbook(ToLog logMethod, Stream zipStream, string templateFile, string jobId)
        {
            V8cExcelWriter ew = new V8cExcelWriter(logMethod);
            string outputFile = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";
            File.Copy(templateFile, outputFile);

            string customCallback = null;

            // Get rid of formula results so they have to be recalced on open
            ew.OpenWorkbook(outputFile);
            ew.FlushCachedValues();
            ew.DeleteCalcChain();
            ew.WriteRange("JobUUID", jobId);

            logMethod("Extracting from ZIP");
            JsonSerializer ser = new JsonSerializer();

            // Get the metadata out first
            using (var zf = new ZipFile(zipStream))
            {
                string fileName = "report/metadata.json";
                var ze = zf.GetEntry(fileName);
                if (ze == null)
                {
                    throw new ArgumentException(fileName, "not found in Zip");
                }
                using (var s = zf.GetInputStream(ze))
                {
                    // Parse the JSON out of the request
                    StreamReader dReader = new StreamReader(s);
                    JsonTextReader myReader = new JsonTextReader(dReader);
                    VerMetadataStructure metaStruct = ser.Deserialize<VerMetadataStructure>(myReader);

                    logMethod("Parsing JSON metadata input. Project is: " + metaStruct.name + "; output workbook type is: " + metaStruct.type);
                    customCallback = metaStruct.callback;

                    ew.WriteRange("Engagement", metaStruct.name);
                    ew.WriteRange("Start_Date", metaStruct.startDate);
                    ew.WriteRange("End_Date", metaStruct.endDate);

                    ew.WriteRange("Core_Application", metaStruct.v8Version);
                    ew.WriteRange("Match_Engine", metaStruct.meVersion);
                    ew.WriteRange("Excel_Plugin", GetMyVersionNumber());
                    ew.WriteRange("Extract_Date", metaStruct.validisUploadDate);

                    //ew.WriteRange("ExtractDate", metaStruct.workpaperDate);
                    //ew.WriteRange("UploadDate", metaStruct.validisUploadDate);

                    ew.SetPivotDateRange("cjPivot", (DateTime)metaStruct.startDate, (DateTime)metaStruct.endDate);

                    if (metaStruct.type == "revenue")
                    {
                        ew.HideSheet("V8 Summary");
                        ew.HideSheet("Expenses");
                        ew.HideSheet("Outflows");
                        ew.HideSheet("AP Sub");
                        ew.HideSheet("ap");
                    }

                    if (metaStruct.type == "expense")
                    {
                        ew.HideSheet("V8 Summary");
                        ew.HideSheet("Revenue");
                        ew.HideSheet("Deferred");
                        ew.HideSheet("Inflows");
                        ew.HideSheet("AR Sub");
                        ew.HideSheet("ar");
                    }

                    if (metaStruct.largeDataSet)
                    {
                        ew.HideSheet("V8 Summary");
                    }
                    else
                    {
                        ew.HideSheet("V8 Summary (lmtd)");
                        ew.HideSheet("AR In");
                    }
                    // Always hide the version sheet
                    ew.HideSheet("version");

                    // Convert to UK?
                    CheckLocale(ew, metaStruct.locale);
                }

                // Go back and stream-unzip the file
                zipStream.Seek(0, SeekOrigin.Begin);
                using (var bigFile = new ZipInputStream(zipStream))
                {
                    ZipEntry nextFile;
                    bool stopReading = false;
                    while (!stopReading)
                    {
                        nextFile = bigFile.GetNextEntry();
                        ew.SaveAndClose(); // see note below
                        if (nextFile == null)
                            stopReading = true;
                        else
                        {
                            // Note: This whole save-and-open thing saves a bit of memory just because other bits of the sheet
                            // aren't held in memory by the SDK. But if it's time consuming we may be able to get rid of it
                            ew.OpenWorkbook(outputFile);
                            if (nextFile.Name == "report/V-BANK_TXNS.json")
                            {
                                logMethod("Parsing JSON bank input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerBankItem> bankStruct = ser.Deserialize<List<VerBankItem>>(myReader);
                                // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                                // rather than the defaults left in the spreadsheet
                                if (bankStruct.Count == 0)
                                    bankStruct.Add(new VerBankItem());
                                using (DataTable dtBank = new DataTable())
                                {

                                    /* 5/19/2019 -- Chris's latest ordering:
                                     * DATE   
                                     * AMOUNT    
                                     * INFLOWS    
                                     * OUTFLOWS    
                                     * TRANSACTION_TYPE    
                                     * CHECK_NO    
                                     * DESCRIPTION    
                                     * TRANSACTION_ID    
                                     * ACCOUNT_TYPE    
                                     * ACCOUNT_NAME   
                                     * FILENAME    
                                     * PAGE    
                                     * MATCH_ID_BANK_ERP   
                                     * F_DATE    
                                     * YEAR    
                                     * QUARTER    
                                     * MONTH
                                     */
                                    dtBank.Columns.Add("V8", typeof(string));
                                    dtBank.Columns.Add("V8_TYPE", typeof(string));
                                    dtBank.Columns.Add("DATE", typeof(DateTime));
                                    dtBank.Columns.Add("AMOUNT", typeof(double));
                                    dtBank.Columns.Add("CURRENCY", typeof(string));
                                    dtBank.Columns.Add("INFLOWS", typeof(double));
                                    dtBank.Columns.Add("OUTFLOWS", typeof(double));
                                    dtBank.Columns.Add("TRANSACTION_TYPE", typeof(string));
                                    dtBank.Columns.Add("CHECK_NO", typeof(string));
                                    dtBank.Columns.Add("DESCRIPTION", typeof(string));
                                    dtBank.Columns.Add("TRANSACTION_ID", typeof(string));
                                    dtBank.Columns.Add("ACCOUNT_TYPE", typeof(string));
                                    dtBank.Columns.Add("ACCOUNT_NAME", typeof(string));
                                    dtBank.Columns.Add("FILENAME", typeof(string));
                                    dtBank.Columns.Add("PAGE", typeof(string));
                                    dtBank.Columns.Add("MATCH_ID_BANK_ERP", typeof(string));
                                    dtBank.AcceptChanges();
                                    foreach (var item in bankStruct)
                                    {
                                        dtBank.Rows.Add(new object[] {
                                                            item.verified,
                                                            item.v8_type,
                                                            item.date,
                                                            item.amount,
                                                            item.accountCurrency,
                                                            item.creditValue,
                                                            item.debitValue,
                                                            item.transactionType,
                                                            item.checkNo,
                                                            item.description,
                                                            item.transactionId,
                                                            item.accountType,
                                                            item.accountName,
                                                            item.filename,
                                                            item.page,
                                                            item.matchIdBankErp });
                                    }
                                    ew.WriteTable("bData", dtBank, null);
                                }
                                //FakeUpExtraData(bankStruct, ser, 1000000 - 12 - bankStruct.Length, nextFile.Name);
                            }

                            if (nextFile.Name == "report/V-GL_TXNS.json")
                            {
                                logMethod("Parsing JSON GL input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerGlItem> glStruct = ser.Deserialize<List<VerGlItem>>(myReader);
                                // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                                // rather than the defaults left in the spreadsheet
                                if (glStruct.Count == 0)
                                    glStruct.Add(new VerGlItem());
                                using (DataTable dtGl = new DataTable())
                                {
                                    /*
                                     * 5/19/2019 -- Chris GL ordering:
                                     *
                                     * V8    
                                     * GL_TX_ID    
                                     * GL_TRANSACTION_DATE    
                                     * GL_BASE_VALUE    
                                     * GL_LINE_DESCRIPTION    
                                     * GL_ACCOUNT_CODE    
                                     * GL_ACCOUNT_NAME   
                                     * GL_PRIMARY_CATEGORY_NAME   
                                     * GL_TAG_CODE    
                                     * GL_SHEET_LABEL    
                                     * GL_PERIOD_END_DATE    
                                     * GL_HEADER_DESCRIPTION   
                                     * GL_HEADER_SOURCE   
                                     * GL_LINE_SOURCE    
                                     * GL_DEBIT_VALUE   
                                     * GL_CREDIT_VALUE   
                                     * GL_TRANSACTION_VALUE    
                                     * AR_SALES_TX_IDS    
                                     * AR_CUSTOMER_NAME    
                                     * AP_PURCHASES_TX_IDS    
                                     * AP_SUPPLIER_NAME    
                                     * BANK_TX_IDS    
                                     * MATCH_ID_DEP_DETAIL   
                                     * TXN_TYPE    
                                     * YEAR    
                                     * QUARTER   
                                     * MONTH
                                     */

                                    dtGl.Columns.Add("V8", typeof(string));
                                    dtGl.Columns.Add("V8_TYPE", typeof(string));
                                    dtGl.Columns.Add("ERP_TX_ID", typeof(string));
                                    dtGl.Columns.Add("GL_TX_ID", typeof(string));
                                    dtGl.Columns.Add("GL_TRANSACTION_DATE", typeof(DateTime));
                                    dtGl.Columns.Add("GL_BASE_VALUE", typeof(double));
                                    dtGl.Columns.Add("GL_LINE_DESCRIPTION", typeof(string));
                                    dtGl.Columns.Add("GL_ACCOUNT_CODE", typeof(string));
                                    dtGl.Columns.Add("GL_ACCOUNT_ID", typeof(double));
                                    dtGl.Columns.Add("GL_ACCOUNT_NAME", typeof(string));
                                    dtGl.Columns.Add("GL_PRIMARY_CATEGORY_NAME", typeof(string));
                                    dtGl.Columns.Add("GL_TAG_CODE", typeof(string));
                                    dtGl.Columns.Add("GL_SHEET_LABEL", typeof(string));
                                    dtGl.Columns.Add("GL_PERIOD_END_DATE", typeof(DateTime));
                                    dtGl.Columns.Add("GL_HEADER_DESCRIPTION", typeof(string));
                                    dtGl.Columns.Add("GL_HEADER_SOURCE", typeof(string));
                                    dtGl.Columns.Add("GL_LINE_SOURCE", typeof(string));
                                    dtGl.Columns.Add("GL_DEBIT_VALUE", typeof(double));
                                    dtGl.Columns.Add("GL_CREDIT_VALUE", typeof(double));
                                    dtGl.Columns.Add("GL_TRANSACTION_VALUE", typeof(double));
                                    dtGl.Columns.Add("BANK_TX_IDS", typeof(string));
                                    dtGl.Columns.Add("TXN_TYPE", typeof(string));
                                    dtGl.Columns.Add("JOURNAL_ENTRY", typeof(bool));
                                    dtGl.AcceptChanges();
                                    foreach (var item in glStruct)
                                    {
                                        dtGl.Rows.Add(new object[] {
                                                        item.verified,
                                                        item.v8Type,
                                                        item.id,
                                                        item.glTxId,
                                                        item.date,
                                                        item.amount,
                                                        item.lineDescription,
                                                        item.accountCode,
                                                        item.accountId,
                                                        item.accountName,
                                                        item.accountCategoryName,
                                                        item.accountCategoryTag,
                                                        item.sheetLabel,
                                                        item.periodEnd,
                                                        item.headerDescription,
                                                        item.headerSource,
                                                        item.lineSource,
                                                        item.debitAmount,
                                                        item.creditAmount,
                                                        item.convertedAmount,
                                                        string.Join(",", item.bankTxnIds),
                                                        item.txnType,
                                                        item.journalEntry
                                    });
                                    }
                                    ew.WriteTable("glData", dtGl, null);
                                    //FakeUpExtraData(glStruct, ser, 1000000 - 12 - glStruct.Length, nextFile.Name);
                                }
                            }

                            if (nextFile.Name == "report/V-AR_TXNS.json")
                            {
                                logMethod("Parsing JSON AR input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerArItem> arStruct = ser.Deserialize<List<VerArItem>>(myReader);
                                // If there's no data, hide this sheet
                                if (arStruct.Count == 0)
                                {
                                    // If we don't hide this sheet, we should do the dummy row thing we do in the others
                                    ew.HideSheet("ar");
                                    ew.HideSheet("AR Sub");
                                }
                                else
                                {
                                    using (DataTable dtAr = new DataTable())
                                    {
                                        dtAr.Columns.Add("V8", typeof(string));
                                        dtAr.Columns.Add("V8_TYPE", typeof(string));
                                        dtAr.Columns.Add("ERP_TX_ID", typeof(string));
                                        dtAr.Columns.Add("BANK_TX_IDS", typeof(string));
                                        dtAr.Columns.Add("GL_TX_ID", typeof(string));
                                        dtAr.Columns.Add("GL_ACCOUNT_NAME", typeof(string));
                                        dtAr.Columns.Add("SALES_TX_ID", typeof(string));
                                        dtAr.Columns.Add("TRANSACTION_TYPE_NAME", typeof(string));
                                        dtAr.Columns.Add("CUSTOMER_DOCUMENT_ID", typeof(string));
                                        dtAr.Columns.Add("CUSTOMER_NAME", typeof(string));
                                        dtAr.Columns.Add("TRANSACTION_DATE", typeof(DateTime));
                                        dtAr.Columns.Add("PERIOD_END_DATE", typeof(DateTime));
                                        dtAr.Columns.Add("ENTRY_TIMESTAMP", typeof(DateTime));
                                        dtAr.Columns.Add("SETTLED_TIMESTAMP", typeof(DateTime));
                                        dtAr.Columns.Add("TRANSACTION_VALUE", typeof(double));
                                        dtAr.Columns.Add("BASE_VALUE", typeof(double));
                                        dtAr.Columns.Add("DEBIT_VALUE", typeof(double));
                                        dtAr.Columns.Add("CREDIT_VALUE", typeof(double));
                                        dtAr.Columns.Add("INVOICE_TX_ID", typeof(string));
                                        dtAr.Columns.Add("INVOICE_DATE", typeof(string));
                                        dtAr.Columns.Add("INV_AMT", typeof(double));
                                        dtAr.Columns.Add("AR_BAL", typeof(double));
                                        dtAr.Columns.Add("SIGN_VALUE", typeof(double));
                                        dtAr.Columns.Add("DAYS_OUT", typeof(double));
                                        dtAr.Columns.Add("AR_AUDIT", typeof(bool));
                                        dtAr.Columns.Add("SUBAR_BAL", typeof(double));
                                        dtAr.Columns.Add("SUB_AMT", typeof(double));
                                        dtAr.AcceptChanges();
                                        foreach (var item in arStruct)
                                        {
                                            dtAr.Rows.Add(new object[] {
                                        item.verified,
                                        item.v8Type,
                                        item.id,
                                        null,
                                        null,
                                        null,
                                        item.custSupTxId,
                                        item.txTypeName,
                                        item.docId,
                                        item.custSupName,
                                        item.date,
                                        item.periodEnd,
                                        null,
                                        item.settledTimestamp,
                                        item.convertedAmount,
                                        item.amount,
                                        item.debitAmount,
                                        item.creditAmount,
                                        item.invTxId,
                                        item.invDate,
                                        item.invAmount,
                                        item.arApBal ?? 0,
                                        item.signValue,
                                        item.invDaysOutstanding,
                                        item.arApAudit,
                                        item.subArApBal,
                                        item.subAmount
                                    });
                                        }
                                        ew.WriteTable("arData", dtAr, null);
                                    }
                                }
                            }

                            if (nextFile.Name == "report/V-AP_TXNS.json")
                            {
                                logMethod("Parsing JSON AP input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerArItem> apStruct = ser.Deserialize<List<VerArItem>>(myReader);
                                // If there's no data, hide this sheet
                                if (apStruct.Count == 0)
                                {
                                    // If we don't hide this sheet, we should do the dummy row thing we do in the others
                                    ew.HideSheet("ap");
                                    ew.HideSheet("AP Sub");
                                }
                                else
                                {
                                    using (DataTable dtAp = new DataTable())
                                    {
                                        dtAp.Columns.Add("V8", typeof(string));
                                        dtAp.Columns.Add("V8_TYPE", typeof(string));
                                        dtAp.Columns.Add("ERP_TX_ID", typeof(string));
                                        dtAp.Columns.Add("BANK_TX_IDS", typeof(string));
                                        dtAp.Columns.Add("GL_TX_ID", typeof(string));
                                        dtAp.Columns.Add("GL_ACCOUNT_NAME", typeof(string));
                                        dtAp.Columns.Add("PURCHASES_TX_ID", typeof(string));
                                        dtAp.Columns.Add("TRANSACTION_TYPE_NAME", typeof(string));
                                        dtAp.Columns.Add("SUPPLIER_DOCUMENT_ID", typeof(string));
                                        dtAp.Columns.Add("SUPPLIER_NAME", typeof(string));
                                        dtAp.Columns.Add("TRANSACTION_DATE", typeof(DateTime));
                                        dtAp.Columns.Add("PERIOD_END_DATE", typeof(DateTime));
                                        dtAp.Columns.Add("ENTRY_TIMESTAMP", typeof(DateTime));
                                        dtAp.Columns.Add("SETTLED_TIMESTAMP", typeof(DateTime));
                                        dtAp.Columns.Add("TRANSACTION_VALUE", typeof(double));
                                        dtAp.Columns.Add("BASE_VALUE", typeof(double));
                                        dtAp.Columns.Add("DEBIT_VALUE", typeof(double));
                                        dtAp.Columns.Add("CREDIT_VALUE", typeof(double));
                                        dtAp.Columns.Add("INVOICE_TX_ID", typeof(string));
                                        dtAp.Columns.Add("INVOICE_DATE", typeof(string));
                                        dtAp.Columns.Add("INV_AMT", typeof(double));
                                        dtAp.Columns.Add("AP_BAL", typeof(double));
                                        dtAp.Columns.Add("SIGN_VALUE", typeof(double));
                                        dtAp.Columns.Add("DAYS_OUT", typeof(double));
                                        dtAp.Columns.Add("AP_AUDIT", typeof(bool));
                                        dtAp.Columns.Add("SUBAP_BAL", typeof(double));
                                        dtAp.Columns.Add("SUB_AMT", typeof(double));
                                        dtAp.AcceptChanges();
                                        foreach (var item in apStruct)
                                        {
                                            dtAp.Rows.Add(new object[] {
                                        item.verified,
                                        item.v8Type,
                                        item.id,
                                        string.Join(",", item.bankTxnIds),
                                        null,
                                        null,
                                        item.custSupTxId,
                                        item.txTypeName,
                                        item.docId,
                                        item.custSupName,
                                        item.date,
                                        item.periodEnd,
                                        null,
                                        item.settledTimestamp,
                                        item.convertedAmount,
                                        item.amount,
                                        item.debitAmount,
                                        item.creditAmount,
                                        item.invTxId,
                                        item.invDate,
                                        item.invAmount,
                                        item.arApBal ?? 0,
                                        item.signValue,
                                        item.invDaysOutstanding,
                                        item.arApAudit,
                                        item.subArApBal,
                                        item.subAmount
                                    });
                                        }
                                        ew.WriteTable("apData", dtAp, null);
                                    }
                                }
                            }

                            if (nextFile.Name == "report/V-CJ_TXNS.json")
                            {
                                logMethod("Parsing JSON CJ input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerGlItem> cjStruct = ser.Deserialize<List<VerGlItem>>(myReader);
                                // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                                // rather than the defaults left in the spreadsheet
                                if (cjStruct.Count == 0)
                                    cjStruct.Add(new VerGlItem());
                                using (DataTable dtCj = new DataTable())
                                {
                                    /* 
                                     * 5/19/2019 -- McCall's latest CJ order:
                                     *
                                     * V8    
                                     * GL_TX_ID    
                                     * GL_TRANSACTION_DATE    
                                     * GL_BASE_VALUE    
                                     * GL_LINE_DESCRIPTION    
                                     * GL_ACCOUNT_CODE   
                                     * GL_ACCOUNT_NAME    
                                     * GL_PRIMARY_CATEGORY_NAME    
                                     * GL_TAG_CODE    
                                     * GL_SHEET_LABEL   
                                     * GL_PERIOD_END_DATE    
                                     * GL_HEADER_DESCRIPTION    
                                     * GL_HEADER_SOURCE   
                                     * GL_LINE_SOURCE    
                                     * GL_DEBIT_VALUE    
                                     * GL_CREDIT_VALUE   
                                     * GL_TRANSACTION_VALUE   
                                     * AR_SALES_TX_IDS    
                                     * AR_CUSTOMER_NAME   
                                     * AP_PURCHASES_TX_IDS    
                                     * AP_SUPPLIER_NAME   
                                     * BANK_TX_IDS    
                                     * MATCH_ID_DEP_DETAIL    
                                     * TXN_TYPE
                                     */

                                    dtCj.Columns.Add("V8", typeof(string));
                                    dtCj.Columns.Add("V8_TYPE", typeof(string));
                                    dtCj.Columns.Add("ERP_TX_ID", typeof(string));
                                    dtCj.Columns.Add("GL_TX_ID", typeof(string));
                                    dtCj.Columns.Add("GL_TRANSACTION_DATE", typeof(DateTime));
                                    dtCj.Columns.Add("GL_BASE_VALUE", typeof(double));
                                    dtCj.Columns.Add("GL_LINE_DESCRIPTION", typeof(string));
                                    dtCj.Columns.Add("GL_ACCOUNT_CODE", typeof(string));
                                    dtCj.Columns.Add("GL_ACCOUNT_NAME", typeof(string));
                                    dtCj.Columns.Add("GL_PRIMARY_CATEGORY_NAME", typeof(string));
                                    dtCj.Columns.Add("GL_TAG_CODE", typeof(string));
                                    dtCj.Columns.Add("GL_SHEET_LABEL", typeof(string));
                                    dtCj.Columns.Add("GL_PERIOD_END_DATE", typeof(DateTime));
                                    dtCj.Columns.Add("GL_HEADER_DESCRIPTION", typeof(string));
                                    dtCj.Columns.Add("GL_HEADER_SOURCE", typeof(string));
                                    dtCj.Columns.Add("GL_LINE_SOURCE", typeof(string));
                                    dtCj.Columns.Add("GL_DEBIT_VALUE", typeof(double));
                                    dtCj.Columns.Add("GL_CREDIT_VALUE", typeof(double));
                                    dtCj.Columns.Add("GL_TRANSACTION_VALUE", typeof(double));
                                    dtCj.Columns.Add("BANK_TX_IDS", typeof(string));
                                    dtCj.Columns.Add("TXN_TYPE", typeof(string));
                                    dtCj.Columns.Add("JOURNAL_ENTRY", typeof(bool));
                                    dtCj.AcceptChanges();
                                    foreach (var item in cjStruct)
                                    {
                                        dtCj.Rows.Add(new object[] {
                                                        item.verified,
                                                        item.v8Type,
                                                        item.id,
                                                        item.glTxId,
                                                        item.date,
                                                        item.amount,
                                                        item.lineDescription,
                                                        item.accountCode,
                                                        item.accountName,
                                                        item.accountCategoryName,
                                                        item.accountCategoryTag,
                                                        item.sheetLabel,
                                                        item.periodEnd,
                                                        item.headerDescription,
                                                        item.headerSource,
                                                        item.lineSource,
                                                        item.debitAmount,
                                                        item.creditAmount,
                                                        item.convertedAmount,
                                                        string.Join(",", item.bankTxnIds),
                                                        item.txnType,
                                                        item.journalEntry
                                    });
                                    }
                                    ew.WriteTable("cjData", dtCj, null);
                                }
                                //FakeUpExtraData(cjStruct, ser, 1000000 - 12 - cjStruct.Length, nextFile.Name);
                            }

                            if (nextFile.Name == "report/V-COMP.json")
                            {
                                logMethod("Parsing JSON COMP input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerCompItem> compStruct = ser.Deserialize<List<VerCompItem>>(myReader);
                                // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                                // rather than the defaults left in the spreadsheet
                                if (compStruct.Count == 0)
                                    compStruct.Add(new VerCompItem());
                                using (DataTable dtComp = new DataTable())
                                {
                                    dtComp.Columns.Add("CODE", typeof(string));
                                    dtComp.Columns.Add("ID", typeof(double));
                                    dtComp.Columns.Add("ACCOUNT", typeof(string));
                                    dtComp.Columns.Add("PRIMARY_CATEGORY", typeof(string));
                                    dtComp.Columns.Add("TAG", typeof(string));
                                    dtComp.Columns.Add("STARTING_BAL", typeof(double));
                                    dtComp.Columns.Add("ACTIVITY", typeof(double));
                                    dtComp.Columns.Add("ENDING_BAL", typeof(double));
                                    dtComp.AcceptChanges();
                                    foreach (var item in compStruct)
                                    {
                                        dtComp.Rows.Add(new object[] {
                                        item.code,
                                        item.accountId,
                                        item.account,
                                        item.primaryCategory,
                                        item.tag,
                                        item.startingBal,
                                        item.activity,
                                        item.endingBalance
                                    });
                                    }
                                    ew.WriteTable("compData", dtComp, null);
                                }
                            }

                            if (nextFile.Name == "report/V-TRACE.json")
                            {
                                logMethod("Parsing JSON TRACE input");
                                // Parse the JSON out of the request
                                StreamReader dReader = new StreamReader(bigFile);
                                JsonTextReader myReader = new JsonTextReader(dReader);
                                List<VerTraceItem> trStruct = ser.Deserialize<List<VerTraceItem>>(myReader);
                                // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                                // rather than the defaults left in the spreadsheet
                                if (trStruct.Count == 0)
                                    trStruct.Add(new VerTraceItem());
                                using (DataTable dtTrace = new DataTable())
                                {
                                    dtTrace.Columns.Add("VERIFIED", typeof(string));
                                    dtTrace.Columns.Add("RECONCILED", typeof(float));
                                    dtTrace.Columns.Add("TRACE_ID", typeof(string));
                                    dtTrace.Columns.Add("GL_UUID", typeof(string));
                                    dtTrace.Columns.Add("GL_TX_ID", typeof(string));
                                    dtTrace.Columns.Add("DATE", typeof(DateTime));
                                    dtTrace.Columns.Add("AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("DEBIT_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("CREDIT_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("CONVERTED_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("ACCOUNT_ID", typeof(float));
                                    dtTrace.Columns.Add("ACCOUNT_NAME", typeof(string));
                                    dtTrace.Columns.Add("ACCOUNT_CODE", typeof(string));
                                    dtTrace.Columns.Add("CATEGORY_NAME", typeof(string));
                                    dtTrace.Columns.Add("CATEGORY_TAG", typeof(string));
                                    dtTrace.Columns.Add("SHEET", typeof(string));
                                    dtTrace.Columns.Add("SOURCE", typeof(string));
                                    dtTrace.Columns.Add("BASE_TYPE", typeof(string));
                                    dtTrace.Columns.Add("ERROR_CODE", typeof(float));
                                    dtTrace.Columns.Add("ERROR_MESSAGE", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_GLUUID", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_GLTXID", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_DATE", typeof(DateTime));
                                    dtTrace.Columns.Add("ENDPOINT_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_DEBIT", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_CREDIT", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_CONVERTED_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_APPLIED_AMOUNT", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_ACCOUNT_ID", typeof(float));
                                    dtTrace.Columns.Add("ENDPOINT_ACCOUNT_CODE", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_ACCOUNT_NAME", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_CATEGORY_TAG", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_CATEGORY_NAME", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_SHEET", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_SOURCE", typeof(string));
                                    dtTrace.Columns.Add("ENDPOINT_BASE_TYPE", typeof(string));
                                    dtTrace.Columns.Add("TRACE_TYPE", typeof(string));

                                    dtTrace.AcceptChanges();
                                    foreach (var item in trStruct)
                                    {
                                        foreach (var endpoint in item.endpoints)
                                        {
                                            dtTrace.Rows.Add(new object[] {
                                        item.verified,
                                        item.reconciled,
                                        item.traceId,
                                        item.glUUID,
                                        item.glTxId,
                                        item.date,
                                        item.amount,
                                        item.debitAmount,
                                        item.creditAmount,
                                        item.convertedAmount,
                                        item.accountId,
                                        item.accountName,
                                        item.accountCode,
                                        item.categoryName,
                                        item.categoryTag,
                                        item.sheet,
                                        item.source,
                                        item.baseType,
                                        item.errorCode,
                                        item.errorMessage,
                                        endpoint.endpointGlUUID,
                                        endpoint.endpointGlTxId,
                                        endpoint.endpointDate,
                                        endpoint.endpointAmount,
                                        endpoint.endpointDebit,
                                        endpoint.endpointCredit,
                                        endpoint.endpointConvertedAmount,
                                        endpoint.endpointAppliedAmount,
                                        endpoint.endpointAccountId,
                                        endpoint.endpointAccountCode,
                                        endpoint.endpointAccountName,
                                        endpoint.endpointCategoryTag,
                                        endpoint.endpointCategoryName,
                                        endpoint.endpointSheet,
                                        endpoint.endpointSource,
                                        endpoint.endpointBaseType,
                                        item.type
                                        });
                                        }
                                    }
                                    ew.WriteTable("traceData", dtTrace, null);
                                }
                            }
                        }
                    }
                    //ew.SaveAndClose(); // see note above
                    return new ReturnStructure() { spreadsheetFile = outputFile, callBackUrl = customCallback };
                }
            }
            throw new Exception("Failed to parse ZIP file");
        }

        private static string GetMyVersionNumber()
        {
            // https://stackoverflow.com/questions/826777/how-to-have-an-auto-incrementing-version-number-visual-studio
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            DateTime buildDate = new DateTime(2000, 1, 1)
                                    .AddDays(version.Build).AddSeconds(version.Revision * 2);
            string displayableVersion = version.ToString() + " (" + buildDate.ToLongDateString() + ")";
            return displayableVersion;
        }

        private static void CheckLocale(V8cExcelWriter ew, string locale)
        {
            if (locale.ToLower() == "en_gb")
                ew.ChangeLocale("£", "809");
        }

#pragma warning disable IDE0051 // Remove unused private members
        private static void FakeUpExtraData(VerGlItem[] glStruct, JsonSerializer ser, int extras, string fName)
#pragma warning restore IDE0051 // Remove unused private members
        {
            VerGlItem[] vi = new VerGlItem[glStruct.Length + extras];
            for (int i = 0; i < vi.Length; i++)
            {
                vi[i] = glStruct[(i >= glStruct.Length ? 0 : i)];
            }
            glStruct = vi;
            using (StreamWriter sw = new StreamWriter(@"C:\Users\Chris\Documents\GitHub\excel-generator-azure\sampleData\verify\" + fName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                ser.Serialize(writer, glStruct);
            }
        }

#pragma warning disable IDE0051 // Remove unused private members
        private static void FakeUpExtraData(VerBankItem[] bankStruct, JsonSerializer ser, int extras, string fName)
#pragma warning restore IDE0051 // Remove unused private members
        {
            VerBankItem[] vi = new VerBankItem[bankStruct.Length + extras];
            for (int i = 0; i < vi.Length; i++)
            {
                vi[i] = bankStruct[(i >= bankStruct.Length ? 0 : i)];
            }
            bankStruct = vi;
            using (StreamWriter sw = new StreamWriter(@"C:\Users\Chris\Documents\GitHub\excel-generator-azure\sampleData\verify\" + fName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                ser.Serialize(writer, bankStruct);
            }
        }

        public static ReturnStructure BuildInspectWorkbook(ToLog logMethod, Stream zipStream, string templateFile)
        {
            V8cExcelWriter ew = new V8cExcelWriter(logMethod);
            string outputFile = Path.GetTempPath() + Guid.NewGuid().ToString() + ".xlsx";
            File.Copy(templateFile, outputFile);
            ew.OpenWorkbook(outputFile);
            ew.FlushCachedValues();
            ew.DeleteCalcChain();

            string customCallback = null;
            DataTable dtAccount = null; // this will be used in the transactions table as well, for some lookups that were too slow in Excel

            //ew.WriteRange("JobUUID", jobId);

            logMethod("Extracting from ZIP");
            JsonSerializer ser = new JsonSerializer();
            // Unzip the file
            using (var bigFile = new ZipInputStream(zipStream))
            {
                ZipEntry nextFile;
                bool stopReading = false;
                while (!stopReading)
                {
                    nextFile = bigFile.GetNextEntry();
                    if (nextFile == null)
                        stopReading = true;
                    else
                    {
                        if (nextFile.Name == "report/metadata.json")
                        {
                            logMethod("Parsing JSON metadata input");
                            // Parse the JSON out of the request
                            StreamReader dReader = new StreamReader(bigFile);
                            JsonTextReader myReader = new JsonTextReader(dReader);
                            InsMetaStructure metaStruct = ser.Deserialize<InsMetaStructure>(myReader);

                            customCallback = metaStruct.metadata.callback;
                            // Convert to UK?
                            CheckLocale(ew, metaStruct.metadata.locale);

                            if (metaStruct.metadata.type != "balances")
                            {
                                //ew.HideColumn("Transactions", "D");
                                ew.HideSheet("Balances");
                                ew.HideSheet("Balance Trending");
                            }
                            // Always hide the version sheet
                            ew.HideSheet("version");
                            ew.HideSheet("Account Summary");

                            ew.WriteRange("Core_Application", metaStruct.metadata.v8Version);
                            ew.WriteRange("Match_Engine", metaStruct.metadata.meVersion);
                            ew.WriteRange("Excel_Plugin", GetMyVersionNumber());
                        }

                        if (nextFile.Name == "report/accounts.json")
                        {
                            logMethod("Parsing JSON accounts input");
                            // Parse the JSON out of the request
                            StreamReader dReader = new StreamReader(bigFile);
                            JsonTextReader myReader = new JsonTextReader(dReader);
                            InsAccountsStructure accounts = ser.Deserialize<InsAccountsStructure>(myReader);
                            // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                            // rather than the defaults left in the spreadsheet
                            if (accounts.data.Count == 0)
                                accounts.data.Add("somekey", new InsAccountItem());
                            dtAccount = new DataTable(); // this is released later in the Transactions read
                            dtAccount.Columns.Add("Bank Name", typeof(string));
                            dtAccount.Columns.Add("Account Type", typeof(string));
                            dtAccount.Columns.Add("Account Name", typeof(string));
                            dtAccount.Columns.Add("Entity", typeof(string));
                            dtAccount.Columns.Add("Account Description", typeof(string));
                            dtAccount.Columns.Add("Account Number", typeof(string));
                            dtAccount.Columns.Add("Beg Bal Date", typeof(DateTime));
                            dtAccount.Columns.Add("Beg Balance", typeof(double));
                            dtAccount.Columns.Add("Inflows", typeof(double));
                            dtAccount.Columns.Add("Outflows", typeof(double));
                            dtAccount.Columns.Add("Net Activity", typeof(double));
                            dtAccount.Columns.Add("End Bal Date", typeof(DateTime));
                            dtAccount.Columns.Add("End Balance", typeof(double));
                            dtAccount.Columns.Add("ID", typeof(string));
                            dtAccount.AcceptChanges();
                            foreach (var item in accounts.data)
                            {
                                dtAccount.Rows.Add(new object[] {
                                        item.Value.bank,
                                        item.Value.accountType,
                                        item.Value.name,
                                        item.Value.entityName,
                                        item.Value.description,
                                        item.Value.number,
                                        item.Value.balanceStartDate,
                                        item.Value.balanceBeginning,
                                        item.Value.inflows,
                                        item.Value.outflows,
                                        item.Value.netActivity,
                                        item.Value.balanceEndDate,
                                        item.Value.balanceEnding,
                                        item.Value.id
                                    });
                            }
                            ew.WriteTable("accounts", dtAccount, null);
                        }

                        if (nextFile.Name == "report/balances.json")
                        {
                            logMethod("Parsing JSON balances input");
                            // Parse the JSON out of the request
                            StreamReader dReader = new StreamReader(bigFile);
                            JsonTextReader myReader = new JsonTextReader(dReader);
                            InsBalancesStructure balances = ser.Deserialize<InsBalancesStructure>(myReader);
                            // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                            // rather than the defaults left in the spreadsheet
                            if (balances.data.Count == 0)
                                balances.data.Add(new InsBalancesItem());
                            using (DataTable dtBalances = new DataTable())
                            {
                                dtBalances.Columns.Add("Date", typeof(DateTime));
                                dtBalances.Columns.Add("Account", typeof(string));
                                dtBalances.Columns.Add("Ending Balance", typeof(double));
                                dtBalances.AcceptChanges();
                                foreach (var item in balances.data)
                                {
                                    foreach (var balItem in item.balances)
                                    {

                                        dtBalances.Rows.Add(new object[] {
                                            balItem.date,
                                            item.name,
                                            balItem.balance
                                        });
                                    }
                                }
                                ew.WriteTable("balances", dtBalances, null);
                            }
                        }

                        if (nextFile.Name == "report/transactions.json")
                        {
                            logMethod("Parsing JSON transactions input");

                            if (dtAccount == null)
                            {
                                throw new Exception("Cannot load transactions.json before account.json");
                            }
                            // Parse the JSON out of the request
                            StreamReader dReader = new StreamReader(bigFile);
                            JsonTextReader myReader = new JsonTextReader(dReader);
                            ExportStructure transactions = ser.Deserialize<ExportStructure>(myReader);
                            // If table is blank, add a new element. This will mean it takes the defaults from the structures in here,
                            // rather than the defaults left in the spreadsheet
                            if (transactions.data.Count == 0)
                                transactions.data.Add(new Transaction());
                            using (DataTable dtAllData = new DataTable())
                            {
                                //Manually create table
                                dtAllData.Columns.Add("Transaction ID", typeof(string));
                                dtAllData.Columns.Add("Date", typeof(DateTime));
                                dtAllData.Columns.Add("Amount", typeof(double));
                                dtAllData.Columns.Add("ABS Amount", typeof(double));
                                dtAllData.Columns.Add("Currency", typeof(string));
                                dtAllData.Columns.Add("Transaction Kind", typeof(string));
                                dtAllData.Columns.Add("Check #", typeof(string));
                                dtAllData.Columns.Add("Description", typeof(string));
                                dtAllData.Columns.Add("Name", typeof(string));
                                dtAllData.Columns.Add("Memo", typeof(string));
                                dtAllData.Columns.Add("Tags", typeof(string));
                                dtAllData.Columns.Add("VisCategory1", typeof(string));
                                dtAllData.Columns.Add("VisCategory2", typeof(string));
                                dtAllData.Columns.Add("VisCategory3", typeof(string));
                                dtAllData.Columns.Add("Custom Field1", typeof(string));
                                dtAllData.Columns.Add("Custom Field2", typeof(string));
                                dtAllData.Columns.Add("Custom Field3", typeof(string));
                                dtAllData.Columns.Add("Custom Field4", typeof(string));
                                dtAllData.Columns.Add("Custom Field5", typeof(string));
                                dtAllData.Columns.Add("Custom Field6", typeof(string));
                                dtAllData.Columns.Add("Custom Field7", typeof(string));
                                dtAllData.Columns.Add("Custom Field8", typeof(string));
                                dtAllData.Columns.Add("Custom Field9", typeof(string));
                                dtAllData.Columns.Add("Match ID", typeof(string));
                                dtAllData.Columns.Add("AccountPeriod ID", typeof(string));
                                dtAllData.Columns.Add("Account ID", typeof(string));
                                dtAllData.Columns.Add("Bank Name", typeof(string));
                                dtAllData.Columns.Add("Account Number", typeof(string));
                                dtAllData.Columns.Add("Account Type", typeof(string));
                                dtAllData.Columns.Add("Account Name", typeof(string));
                                dtAllData.Columns.Add("Account Entity", typeof(string));
                                dtAllData.Columns.Add("File Name", typeof(string));
                                dtAllData.Columns.Add("Page Number", typeof(int));
                                dtAllData.AcceptChanges();


                                int stopAdding = 0; // limiting data size, just for testing. Set to zero to allow all

                                // Find all the category names
                                Dictionary<string, string> renameCats = new Dictionary<string, string>();
                                FindCategoryNames(renameCats, transactions, "visCategories", "VisCategory");
                                FindCategoryNames(renameCats, transactions, "customFields", "Custom Field");

                                foreach (var item in transactions.data)
                                {
                                    Dictionary<string, string> visCatLookup = new Dictionary<string, string>();
                                    int onVisCat = 0;
                                    foreach (Dictionary<string, string> thisOne in item.visCategories)
                                    {
                                        foreach (var pair in thisOne)
                                        {
                                            onVisCat++;
                                            visCatLookup.Add("VisCategory" + onVisCat, pair.Value);
                                        }
                                    }
                                    int onCfCat = 0;
                                    foreach (Dictionary<string, string> thisOne in item.customFields)
                                    {
                                        foreach (var pair in thisOne)
                                        {
                                            onCfCat++;
                                            visCatLookup.Add("Custom Field" + onCfCat, pair.Value);
                                        }
                                    }
                                    // Look up the various columns that were originally lookups in the spreadsheet but that was too slow
                                    dtAccount.PrimaryKey = new DataColumn[] { dtAccount.Columns["ID"] };
                                    DataRow dtRes = dtAccount.Rows.Find(item.accountId);

                                    dtAllData.Rows.Add(new object[] {
                                        item.id,
                                        item.date,
                                        item.amount,
                                        (item.amount == null ? 0 : Math.Abs((double)item.amount)),
                                        item.currency,
                                        item.transactionKind,
                                        item.checkNo,
                                        item.description,
                                        item.name,
                                        item.memo,
                                        String.Join(",",item.tags),
                                        (visCatLookup.ContainsKey("VisCategory1") ? visCatLookup["VisCategory1"] : null),
                                        (visCatLookup.ContainsKey("VisCategory2") ? visCatLookup["VisCategory2"] : null),
                                        (visCatLookup.ContainsKey("VisCategory3") ? visCatLookup["VisCategory3"] : null),
                                        (visCatLookup.ContainsKey("Custom Field1") ? visCatLookup["Custom Field1"] : null),
                                        (visCatLookup.ContainsKey("Custom Field2") ? visCatLookup["Custom Field2"] : null),
                                        (visCatLookup.ContainsKey("Custom Field3") ? visCatLookup["Custom Field3"] : null),
                                        (visCatLookup.ContainsKey("Custom Field4") ? visCatLookup["Custom Field4"] : null),
                                        (visCatLookup.ContainsKey("Custom Field5") ? visCatLookup["Custom Field5"] : null),
                                        (visCatLookup.ContainsKey("Custom Field6") ? visCatLookup["Custom Field6"] : null),
                                        (visCatLookup.ContainsKey("Custom Field7") ? visCatLookup["Custom Field7"] : null),
                                        (visCatLookup.ContainsKey("Custom Field8") ? visCatLookup["Custom Field8"] : null),
                                        (visCatLookup.ContainsKey("Custom Field9") ? visCatLookup["Custom Field9"] : null),
                                        item.matchId,
                                        item.accountPeriodId,
                                        item.accountId, 
                                        (dtRes == null ? "" : dtRes["Bank Name"]),
                                        (dtRes == null ? "" : dtRes["Account Number"]),
                                        (dtRes == null ? "" : dtRes["Account Type"]),
                                        (dtRes == null ? "" : dtRes["Account Name"]),
                                        (dtRes == null ? "" : dtRes["Entity"]),
                                        item.filename,
                                        item.page
                                    });
                                    if (--stopAdding == 0)
                                        break;
                                }

                                ew.WriteTable("data", dtAllData, renameCats);
                            }
                            dtAccount.Dispose(); // this is the point at which we don't need the accounts table
                        }
                    }
                }
                ew.SaveAndClose();
                return new ReturnStructure() { spreadsheetFile = outputFile, callBackUrl = customCallback };
            }
            throw new Exception("Failed to parse ZIP file");
        }

        private static void FindCategoryNames(Dictionary<string, string> renameCats, ExportStructure transactions, string fieldName, string oldColName)
        {
            int highestCat = 0;
            foreach (Transaction item in transactions.data)
            {
                List<Dictionary<string, string>> theList = (List <Dictionary<string, string>>)item.GetType().GetField(fieldName).GetValue(item);
                foreach (Dictionary<string, string> thisOne in theList)
                {
                    foreach (var pair in thisOne)
                    {
                        if (!renameCats.ContainsValue(pair.Key))
                        {
                            highestCat++;
                            renameCats.Add(oldColName + highestCat.ToString(), pair.Key);
                        }
                    }
                }
            }
        }
    }
}