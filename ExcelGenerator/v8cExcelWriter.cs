﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ExcelGenerator
{
    public class V8cExcelWriter
    {
        private SpreadsheetDocument _doc;
        public delegate void ToLog(string message);
        readonly ToLog _logHandler;
        private static readonly List<Type> numericTypes = new List<Type>(new Type[] { typeof(short), typeof(int), typeof(long), typeof(float), typeof(double), typeof(decimal), typeof(ushort), typeof(uint), typeof(ulong) });

        public void PasteTableTable(Table t, DataTable dt, WorksheetPart worksheetPart, string column, uint rowIndex, WorksheetPart replacementPart, string newTablePartRel, string newDrawingPartRel, Dictionary<string, string> renameCategories)
        {
            //WorkbookStylesPart styles = SpreadsheetReader.GetWorkbookStyles(spreadsheet);
            SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

            // Reset all the captions to blank on the data table, because we're about to use them for something else
            foreach (DataColumn dc in dt.Columns)
                dc.Caption = "";
            
            // Expand data table to include extra columns if there are any
            foreach (TableColumn tCol in t.TableColumns)
            {
                if (!dt.Columns.Contains(tCol.Name))
                {
                    if (tCol.CalculatedColumnFormula != null)
                    {
                        _logHandler(t.DisplayName + ": Detected calculated column: " + tCol.Name);
                        // Guid is just an unused type we use to flag formulas
                        DataColumn d = new DataColumn(tCol.Name, typeof(Guid))
                        {
                            // Just using the Caption property to store the formula
                            Caption = tCol.CalculatedColumnFormula.InnerText
                        };
                        dt.Columns.Add(d);
                    } else
                    {
                        // For some reason there's other static data in a column we're not using
                        _logHandler(t.DisplayName + ": Detected (and removed) static data column: " + tCol.Name);
                        DataColumn d = new DataColumn(tCol.Name, typeof(Guid))
                        {
                            // Just using the Caption property to store the formula
                            Caption = ""
                        };
                        dt.Columns.Add(d);
                    }
                }
            }

            // Check column names are as expected and rename if necessary (now that we've added all the calculated ones)
            CheckColumnHeadings(dt, worksheetPart, column, rowIndex, t, renameCategories);

            // Find all existing cell values in the workbook
            CollectExistingCells(worksheetPart, out Dictionary<string, Cell> existingCells, out int firstCol, out int lastCol, out UInt32Value firstRow, out UInt32Value lastRow, renameCategories);
            //_logHandler(t.DisplayName + ": Bounds: " + SpreadsheetReader.getRangeRef(firstCol, firstRow, lastCol, lastRow) + " after parsing original data");

            // Find the bounds of the data table
            int firstDtCol = SpreadsheetReader.GetColumnIndex(column);
            UInt32Value firstDtRow = Convert.ToUInt32(rowIndex + 1); // remember the headers
            if (firstDtCol < firstCol) firstCol = firstDtCol;
            if (firstDtRow < firstRow) firstRow = firstDtRow;

            int lastDtCol = firstDtCol + dt.Columns.Count - 1;
            UInt32Value lastDtRow = firstDtRow + Convert.ToUInt32(dt.Rows.Count) - 1;
            if (lastDtCol > lastCol) lastCol = lastDtCol;
            if (lastDtRow > lastRow) lastRow = lastDtRow;
            //_logHandler(t.DisplayName + ": Bounds: " + SpreadsheetReader.getRangeRef(firstCol, firstRow, lastCol, lastRow) + " after including new data");
            long numCells = (lastCol - firstCol + 1) * (lastRow - firstRow + 1);
            string numCellsT;
            if (numCells > 1000000) {
                numCellsT = (numCells / 1000000).ToString() + "m";
            } else {
                if (numCells > 1000)
                    numCellsT = (numCells / 1000).ToString() + "k";
                else
                    numCellsT = numCells.ToString();
            }
            _logHandler(t.DisplayName + ": Writing " + numCellsT + " cells");
            // Is this too much?
            if (lastRow - firstRow > 999999)
                throw new Exception(t.DisplayName + ": Cannot create workbook with more than one million rows.");

            // Duplicate input and output sheets via SASS-style parsing
            OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
            OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

            Row r = new Row();
            Cell c;

            // Make copies of the top-row cells so we can pinch their styles - they have to exist in the cache
            Dictionary<int, Cell> existingColStyles = GetTopRowStyles(existingCells, firstDtCol, firstDtRow, lastDtCol);

            //_logHandler("BEFORE Stream: " + (GC.GetTotalMemory(true) / 1024 / 1024).ToString() + "mb");
            while (reader.Read())
            { 
                if (reader.ElementType == typeof(SheetData))
                {
                    if (reader.IsEndElement)
                        continue;
                    writer.WriteStartElement(new SheetData());

                    for (UInt32Value row = firstRow; row <= lastRow; row++)
                    {
                        r.RowIndex = row;
                        writer.WriteStartElement(r);
                        for (int col = firstCol; col <= lastCol; col++)
                        {
                            string cellRef = SpreadsheetReader.GetColumnName(col, 0) + row.ToString();
                            // Is this within the bounds of our data table?
                            if ((col >= firstDtCol) && (col <= lastDtCol) && (row >= firstDtRow) && (row <= lastDtRow))
                            {
                                c = existingColStyles[col];

                                DataColumn dataColumn = dt.Columns[col - firstDtCol];
                                object dtItem = dt.Rows[(int)(row - firstDtRow)].ItemArray[dataColumn.Ordinal];
                                c = PopulateTableCell(c, dataColumn, dtItem);
                                if (c != null) // if this was blank, we don't have a cell to write
                                {
                                    c.CellReference = cellRef;
                                    // If we were writing a cell that is NOT a calculated column but somehow still contains a formula,
                                    // get rid of it (thanks, Chris McCall)
                                    if ((c.CellFormula != null) && (string.IsNullOrEmpty(dataColumn.Caption)))
                                        c.CellFormula.Remove();
                                    // If we are writing an array formula, change the ref element to be the correct one
                                    if ((c.CellFormula != null) && (c.CellFormula.FormulaType != null) && (c.CellFormula.FormulaType == CellFormulaValues.Array))
                                        c.CellFormula.Reference = cellRef;
                                    writer.WriteElement(c);
                                }
                            }
                            else
                            {
                                // Is this something directly below our data table (in which case throw it away)?
                                if (!((col >= firstDtCol) && (col <= lastDtCol) && (row >= lastDtRow)))
                                {
                                    // Is this a cached value?
                                    if (existingCells.ContainsKey(cellRef))
                                        writer.WriteElement(existingCells[cellRef]);
                                }
                            }
                        }

                        writer.WriteEndElement(); // row
                    }

                    writer.WriteEndElement(); // sheetdata
                }
                else
                {
                    // If this was a table, update the relationship to the new RelId
                    if (reader.ElementType == typeof(TablePart))
                    {
                        if (reader.IsEndElement)
                            continue;
                        TablePart nTp = new TablePart
                        {
                            Id = newTablePartRel
                        };
                        writer.WriteElement(nTp);
                    }
                    else
                    {
                        // If this was a drawing, update the relationship to the new RelId
                        if (reader.ElementType == typeof(Drawing))
                        {
                            if (reader.IsEndElement)
                                continue;
                            if (newDrawingPartRel == null)
                                throw new Exception("We didn't think this sheet had a drawing part attached to it, but apparently it has a drawing in it?!");
                            Drawing nd = new Drawing
                            {
                                Id = newDrawingPartRel
                            };
                            writer.WriteElement(nd);
                        }
                        else
                        {
                            // Ignore existing cell data
                            if ((reader.ElementType != typeof(Row))
                            && (reader.ElementType != typeof(Cell))
                            && (reader.ElementType != typeof(CellFormula))
                            && (reader.ElementType != typeof(CellValue)))
                            {
                                if (reader.IsStartElement)
                                {
                                    writer.WriteStartElement(reader);
                                    // If there was inline text, do that too
                                    string inlineText = reader.GetText();
                                    if (!string.IsNullOrEmpty(inlineText))
                                        writer.WriteString(inlineText);
                                }
                                else if (reader.IsEndElement)
                                {
                                    writer.WriteEndElement();
                                }
                            }
                        }
                    }
                }
            }

            reader.Close();
            writer.Close();
            reader.Dispose();
            writer.Dispose();
            //_logHandler("AFTER Stream: " + (GC.GetTotalMemory(true) / 1024 / 1024).ToString() + "mb");
        }

        internal void ChangeLocale(string currencySymbol, string localeCode)
        {
            string fixFormat(string oldFormat)
            {
                // Need to replace (using £ as an example):
                //   "$" with £
                //   [$$-409] with [$£-809] - locale with currency symbol
                //   [$-409] with [$-809] - locale without currency symbol
                return oldFormat.Replace("\"$\"", "\"" + currencySymbol + "\"").Replace("[$$-409]", "[$" + currencySymbol + "-" + localeCode + "]").Replace("[$-409]", "[$-" + localeCode + "]");
            }
            WorkbookStylesPart sp = _doc.WorkbookPart.GetPartsOfType<WorkbookStylesPart>().First();

            foreach (NumberingFormat fmt in sp.Stylesheet.NumberingFormats.ChildElements)
            {
                fmt.FormatCode = fixFormat(fmt.FormatCode);
                //Console.WriteLine(fmt.FormatCode);
            }

            foreach (DifferentialFormat dfmt in sp.Stylesheet.DifferentialFormats.ChildElements)
            {
                if (dfmt.NumberingFormat != null)
                {
                    NumberingFormat fmt = dfmt.NumberingFormat;
                    fmt.FormatCode = fixFormat(fmt.FormatCode);
                    //Console.WriteLine(fmt.FormatCode);
                }
            }
        }

        internal void SetSlicer(string slicerName, string[] useVals)
        {
            // Set a slicer

            SlicerCachePart scp = null;
            // Find the appropriate slicer cache definition
            foreach (SlicerCachePart i in _doc.WorkbookPart.GetPartsOfType<SlicerCachePart>())
            {
                if (i.SlicerCacheDefinition.SourceName == slicerName)
                {
                    scp = i;
                }
            }
            if (scp == null)
                throw new Exception("Slicer " + slicerName + " doesn't exist");

            string pCacheId = scp.SlicerCacheDefinition.SlicerCacheData.TabularSlicerCache.PivotCacheId;

            PivotTableCacheDefinitionPart pcp = null;
            // Find the appropriate pivot cache definition
            foreach (PivotTableCacheDefinitionPart i in _doc.WorkbookPart.GetPartsOfType<PivotTableCacheDefinitionPart>())
            {
                // The pivotCacheId is in an extension list - given the complete and utter lack of documentation about these,
                // this was the best way I could see of getting it out
                var pcExt = i.PivotCacheDefinition.PivotCacheDefinitionExtensionList.Elements<PivotCacheDefinitionExtension>();
                DocumentFormat.OpenXml.Office2010.Excel.PivotCacheDefinition npcd = (DocumentFormat.OpenXml.Office2010.Excel.PivotCacheDefinition) pcExt.FirstOrDefault().FirstChild;
                
                if (npcd.PivotCacheId == pCacheId)
                {
                    pcp = i;
                }
            }

            // We now have a slicer cache and the associated pivot cache
            CacheField cf = pcp.PivotCacheDefinition.CacheFields.Elements<CacheField>().Where(c => c.Name == slicerName).First();
            SharedItems shi = cf.SharedItems;
            DocumentFormat.OpenXml.Office2010.Excel.TabularSlicerCacheItems scis = scp.SlicerCacheDefinition.SlicerCacheData.TabularSlicerCache.TabularSlicerCacheItems;

            // Zero the "selected" count for all the existing slicer values
            foreach (DocumentFormat.OpenXml.Office2010.Excel.TabularSlicerCacheItem scItem in scis)
            {
                scItem.IsSelected = false;
            }

            // Put random data into the existing SharedItems so that we don't end up with dupes when we add ours
            foreach (StringItem shis in shi)
            {
                shis.Val = Guid.NewGuid().ToString();
            }

            foreach (string useVal in useVals)
            {
                StringItem sti = new StringItem();
                DocumentFormat.OpenXml.Office2010.Excel.TabularSlicerCacheItem tci = new DocumentFormat.OpenXml.Office2010.Excel.TabularSlicerCacheItem
                {
                    IsSelected = true,
                    Atom = (shi.Count++)
                };
                scis.AppendChild(tci);
                sti.Val = useVal;
                shi.AppendChild(sti);
            }
            //pcp.PivotCacheDefinition.Save();
        }

        internal void UpdateSlicerDisplayName(string realName, string showName)
        {
            // Find the appropriate slicer cache definition
            foreach (Sheet sheet in _doc.WorkbookPart.Workbook.GetFirstChild<Sheets>())
            {
                WorksheetPart worksheetPart = (WorksheetPart)_doc.WorkbookPart.GetPartById(sheet.Id);
                var sParts = worksheetPart.GetPartsOfType<SlicersPart>();
                foreach (SlicersPart i in sParts)
                {
                    foreach (DocumentFormat.OpenXml.Office2010.Excel.Slicer s in i.Slicers)
                    {
                        if (s.Name == realName)
                            s.Caption = showName;
                    }
                }
            }
        }

        public void FlushCachedValues()
        {
            _logHandler("Removing cached cell values from workbook");
            // http://stackoverflow.com/a/13175602/498949
            _doc.WorkbookPart.WorksheetParts
               .SelectMany(part => part.Worksheet.Elements<SheetData>())
               .SelectMany(data => data.Elements<Row>())
               .SelectMany(row => row.Elements<Cell>())
               .Where(cell => cell.CellFormula != null && cell.CellValue != null)
               .ToList()
               .ForEach(cell => cell.CellValue.Remove());
            _logHandler("- Removed");
        }

        private static Cell PopulateTableCell(Cell c, DataColumn dataColumn, object dtItem)
        {
            if (dataColumn.DataType == typeof(DateTime))
            {
                if (dtItem == DBNull.Value)
                    c = null;
                else
                {
                    DateTime value = (DateTime)dtItem;
                    c.CellValue = new CellValue(WorksheetWriter.GetNumericDate(value));
                    c.DataType = new EnumValue<CellValues>(CellValues.Number);
                }
            }
            else
            {
                // Set the value
                if (numericTypes.Contains(dataColumn.DataType))
                {
                    c.CellValue = new CellValue(SpreadsheetWriter.ToXmlNumeric(dtItem));
                }
                else if (dataColumn.DataType == typeof(bool))
                {
                    c.CellValue = new CellValue(SpreadsheetWriter.ToXmlBoolean(dtItem));
                }
                else if (dataColumn.DataType == typeof(Guid)) // this is a calculated column - remove the cell value if there is one
                {
                    c.CellValue?.Remove();
                }
                else {
                    if (dtItem.ToString().Length == 0)
                        c = null; // blank means blank!
                    else
                    {
                        string tidyVal = Regex.Replace(dtItem.ToString(), @"\p{C}+", " ");
                        c.CellValue = new CellValue(tidyVal);
                    }
                }

                if (c != null)
                {
                    // Set the type
                    if (numericTypes.Contains(dataColumn.DataType))
                    {
                        c.DataType = new EnumValue<CellValues>(CellValues.Number);
                    }
                    else if (dataColumn.DataType == typeof(bool))
                    {
                        c.DataType = new EnumValue<CellValues>(CellValues.Boolean); //requires string type
                    }
                    else
                    {
                        c.DataType = new EnumValue<CellValues>(CellValues.String);
                    }
                }
            }

            //if ((c != null) && (c.CellValue != null))
            //    Console.WriteLine("Filling " + c.CellReference + " with " + c.CellValue.InnerText);
            return c;
        }

        private static Dictionary<int, Cell> GetTopRowStyles(Dictionary<string, Cell> existingCells, int firstDtCol, UInt32Value firstDtRow, int lastDtCol)
        {
            Dictionary<int, Cell> existingColStyles = new Dictionary<int, Cell>();
            for (int col = firstDtCol; col <= lastDtCol; col++)
            {
                //string cellRef = ((char)(col + 64)).ToString() + firstDtRow.ToString();
                string cellRef = SpreadsheetReader.GetColumnName(col, 0) + firstDtRow.ToString();
                if (!existingCells.ContainsKey(cellRef))
                    throw new Exception("There are non-populated cells in the top row of data table at " + cellRef);
                existingColStyles.Add(col, existingCells[cellRef]);
            }

            return existingColStyles;
        }

        private static void CollectExistingCells(WorksheetPart worksheetPart, out Dictionary<string, Cell> existingCells, out int firstCol, out int lastCol, out UInt32Value firstRow, out UInt32Value lastRow, Dictionary<string, string> renameColumns)
        {
            using (OpenXmlReader cellReader = OpenXmlReader.Create(worksheetPart))
            {

                existingCells = new Dictionary<string, Cell>();
                firstCol = int.MaxValue;
                lastCol = 0;
                firstRow = int.MaxValue;
                lastRow = 0;
                while (cellReader.Read())
                {
                    if (cellReader.ElementType == typeof(Cell))
                        if (cellReader.HasAttributes)
                        {
                            string cRef = cellReader.Attributes.First(a => a.LocalName == "r").Value;
                            Cell cCell = (Cell)cellReader.LoadCurrentElement();
                            // If it has a formula, remove the value attached to it to force a recalc, because otherwise for some reason
                            // cells with table-style references in them (e.g. to SUM a table) don't seem to recalculate
                            if (cCell.CellFormula != null)
                            {
                                if (cCell.CellValue != null)
                                    cCell.CellValue.Remove();
                                // Are we renaming any columns? If so we have to rewrite all the formulas
                                if (renameColumns != null)
                                {
                                    foreach (var rCat in renameColumns)
                                    {
                                        cCell.CellFormula.Text = cCell.CellFormula.Text.Replace("[" + rCat.Key + "]", "[" + rCat.Value + "]");
                                    }
                                }
                            }
                            existingCells.Add(cRef, cCell);
                            //Console.WriteLine("Existing cell ref: " + cRef);
                            int col = SpreadsheetReader.GetColumnIndex(SpreadsheetReader.ColumnFromReference(cRef));
                            UInt32Value row = SpreadsheetReader.RowFromReference(cRef);
                            if (col < firstCol) firstCol = col;
                            if (col > lastCol) lastCol = col;
                            if (row < firstRow) firstRow = row;
                            if (row > lastRow) lastRow = row;
                        }
                }
            }
        }

        private void CheckColumnHeadings(DataTable dt, WorksheetPart worksheetPart, string column, uint rowIndex, Table t, Dictionary<string, string> renameCategories)
        {
            string colCheckString = column;
            foreach (System.Data.DataColumn dataColumn in dt.Columns)
            {
                string cellReference = (colCheckString + rowIndex).ToString();
                string cellValue = WorksheetWriter.GetCellValue(cellReference, _doc.WorkbookPart, worksheetPart);
                if (!cellValue.Equals(dataColumn.ColumnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new Exception("Column name '" + dataColumn.ColumnName + "' in data is not present in the data table '" + t.DisplayName + "' (or is in the wrong order)");
                }

                if ((renameCategories != null) && (renameCategories.ContainsKey(dataColumn.ColumnName)))
                {
                    string newName = renameCategories[dataColumn.ColumnName];
                    Cell cell = WorksheetWriter.FindCell(colCheckString, rowIndex, worksheetPart);
                    WorksheetWriter.PasteSharedText(_doc, worksheetPart, colCheckString, rowIndex, newName);
                    // Fix the table name too
                    foreach (TableColumn tc in t.TableColumns)
                    {
                        if (tc.Name == dataColumn.ColumnName)
                            tc.Name = newName;
                        if (tc.CalculatedColumnFormula != null)
                            tc.CalculatedColumnFormula.Text = tc.CalculatedColumnFormula.Text.Replace("[" + dataColumn.ColumnName + "]", "[" + newName + "]");
                    }
                }

                //Get the next column
                colCheckString = SpreadsheetReader.GetColumnName(colCheckString, 1);
            }
            _doc.Save(); // We may have changed some column headings
        }

        public V8cExcelWriter(ToLog logHandler)
        {
            _logHandler = logHandler;
        }

        public void OpenWorkbook(string fName)
        {
            _logHandler("Opening " + fName);
            _doc = SpreadsheetDocument.Open(fName, true);
            _logHandler("- Opened");
        }

        public void OpenWorkbook(MemoryStream stream)
        {
            _logHandler("Opening file from stream");
            _doc = SpreadsheetDocument.Open(stream, true);
            _logHandler("- Opened");
        }

        public void WriteRange(string name, double val)
        {
            _logHandler("Writing number " + val.ToString() + " into range " + name);
            CellReference cell = GetRefFromName(name);
            WorksheetWriter wr = new WorksheetWriter(_doc, SpreadsheetReader.GetWorksheetPartByName(_doc, cell.sheetName));
            wr.PasteNumber(cell.localReference, val.ToString());
        }

        public void WriteRange(string name, DateTime val)
        {
            _logHandler("Writing date " + val.ToString() + " into range " + name);
            CellReference cell = GetRefFromName(name);
            WorksheetWriter wr = new WorksheetWriter(_doc, SpreadsheetReader.GetWorksheetPartByName(_doc, cell.sheetName));
            wr.PasteDate(cell.localReference, val);
        }

        public void WriteRange(string name, string val)
        {
            _logHandler("Writing string " + val.ToString() + " into range " + name);
            CellReference cell = GetRefFromName(name);
            if (cell.localReference == "#REF")
                throw new Exception("Named range '" + name + "' points to an invalid reference");
            WorksheetWriter wr = new WorksheetWriter(_doc, SpreadsheetReader.GetWorksheetPartByName(_doc, cell.sheetName));
            wr.PasteText(cell.localReference, val);
        }

        public void WriteRangeHyperlink(string name, string val)
        {
            _logHandler("Writing hyperlink " + val.ToString() + " into range " + name);
            CellReference cell = GetRefFromName(name);
            WorksheetPart wp = SpreadsheetReader.GetWorksheetPartByName(_doc, cell.sheetName);
            WorksheetWriter wr = new WorksheetWriter(_doc, wp);
            wr.PasteText(cell.localReference, val);
            Hyperlink toUpd = wp.Worksheet.Descendants<Hyperlink>()
                .Where(s => s.Reference.Value.StartsWith(cell.localReferenceNoDollars + ":")).First();

            string relationId = toUpd.Id;
            if (!string.IsNullOrEmpty(relationId))
            {
                // get current relation
                HyperlinkRelationship hr = wp.HyperlinkRelationships.Where(a => a.Id == relationId).FirstOrDefault();
                if (hr != null)
                // remove current relation
                { wp.DeleteReferenceRelationship(hr); }
                //add new relation with same Id , but new path
                wp.AddHyperlinkRelationship(new System.Uri(val, System.UriKind.Absolute), true, relationId);
            }
        }

        internal void HideSheet(string sName)
        {
            _logHandler("Hiding sheet " + sName);
            foreach (Sheet sheet in _doc.WorkbookPart.Workbook.GetFirstChild<Sheets>())
            {
                if (sheet.Name == sName)
                {
                    sheet.State = new EnumValue<SheetStateValues> { Value = SheetStateValues.VeryHidden };
                    return;
                }
            }
            throw new Exception("Sheet " + sName + " does not exist and can't be hidden");
        }

        public void WriteTable(string name, DataTable dt, Dictionary<string, string> renameCategories)
        {
            //_logHandler("Writing data into table " + name);

            // Look through all the sheets for tables
            Table t = null;
            TableDefinitionPart tp = null;
            WorksheetPart wp = null;
            foreach (Sheet sheet in _doc.WorkbookPart.Workbook.GetFirstChild<Sheets>())
            {
                WorksheetPart worksheetPart = (WorksheetPart)_doc.WorkbookPart.GetPartById(sheet.Id);
                foreach (TableDefinitionPart tableDefinitionPart in worksheetPart.TableDefinitionParts)
                {
                    if (tableDefinitionPart.Table.DisplayName == name)
                    {
                        wp = worksheetPart;
                        t = tableDefinitionPart.Table;
                        tp = tableDefinitionPart;
                    }
                }
            }

            if (t == null)
                throw new Exception(name + ": Table does not exist");

            WorkbookPart workbookPart = _doc.WorkbookPart;
            string originalSheetId = workbookPart.GetIdOfPart(wp);

            WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
            string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

            string newTablePartRel = replacementPart.CreateRelationshipToPart(tp);
            Sheet renSheet = workbookPart.Workbook.Descendants<Sheet>()
                .Where(s => s.Id.Value.Equals(originalSheetId)).First();
            renSheet.Id.Value = replacementPartId;

            string newDrawingPartRel = null;
            if (wp.DrawingsPart != null)
            {
                newDrawingPartRel = replacementPart.CreateRelationshipToPart(wp.DrawingsPart);
            }

            // Get the full extent of the table
            string tableRange = SpreadsheetReader.ReferenceFromRange(t.Reference);

            _logHandler(name + ": New sheet copy of " + wp.Uri + " is " + replacementPart.Uri);

            PasteTableTable(t, dt, wp, SpreadsheetReader.ColumnFromReference(tableRange), SpreadsheetReader.RowFromReference(tableRange), replacementPart, newTablePartRel, newDrawingPartRel, renameCategories);

            workbookPart.DeletePart(wp);
            //workbookPart.Workbook.Save();

            // Update the table reference
            string lastCol = SpreadsheetReader.GetColumnName(SpreadsheetReader.ColumnFromReference(tableRange), dt.Columns.Count - 1);
            string brRef = tableRange + ":" + lastCol + (SpreadsheetReader.RowFromReference(tableRange) + dt.Rows.Count).ToString();
            t.Reference.InnerText = brRef;
            //t.Save();
        }

        internal void SetPivotDateRange(string ptName, DateTime fromDate, DateTime toDate)
        {
            _logHandler("Setting date filter on PivotTable " + ptName + " to " + fromDate.ToShortDateString() + "-" + toDate.ToShortDateString());
            PivotTablePart ptp = null;
            foreach (WorksheetPart wp in _doc.WorkbookPart.WorksheetParts)
            {
                foreach (PivotTablePart ptpl in wp.PivotTableParts)
                {
                    if (ptpl.PivotTableDefinition.Name.InnerText.Equals(ptName)) // this is the target pivot
                    {
                        ptp = ptpl;
                    }
                }
            }

            if (ptp == null)
                throw new Exception("PivotTable " + ptName + " doesn't exist");
            else
            {
                // Find the relevant filter
                var x = ptp.PivotTableDefinition.Descendants<PivotFilter>().ToList();
                if (ptp.PivotTableDefinition.PivotFilters == null)
                    throw new Exception("Trying to set a date filter on a PivotTable that doesn't have one");
                else
                {
                    IEnumerable<PivotFilter> fs = ptp.PivotTableDefinition.PivotFilters.Descendants<PivotFilter>().Where(c => c.Type.InnerText.Equals("dateBetween"));
                    // Just relying on that dateBetween filter having both a start and end date
                    IEnumerable<CustomFilter> ffrom = fs.First().Descendants<CustomFilter>().Where(c => c.Operator.InnerText.Equals("greaterThanOrEqual"));
                    ffrom.First().Val = WorksheetWriter.GetNumericDate(fromDate);
                    IEnumerable<CustomFilter> fto = fs.First().Descendants<CustomFilter>().Where(c => c.Operator.InnerText.Equals("lessThanOrEqual"));
                    fto.First().Val = WorksheetWriter.GetNumericDate(toDate);
                }
            }
        }

        private CellReference GetRefFromName(string name)
        {
            DefinedName dn = SpreadsheetReader.GetDefinedName(_doc, name);
            if (dn == null)
                throw new Exception("Range name '" + name + "' does not exist in template workbook");
            else
                return CellReference.FromDefinedName(dn);
        }

        public void DeleteCalcChain()
        {
            _logHandler("Deleting calc chain");
            // We need to delete the calc chain - right now this is just because we remove formulas from stuff in data tables, but there might be
            // other reasons that the calc chain ends up busted
            CalculationChainPart calculationChainPart = _doc.WorkbookPart.CalculationChainPart;
            _doc.WorkbookPart.DeletePart(calculationChainPart);
        }

        public void SaveAndClose()
        {
            _logHandler("Saving workbook");
            _doc.Close();
            _doc.Dispose();
            _logHandler("- Saved");
        }

        internal void HideColumn(string sName, string col)
        {
            _logHandler("Hiding column " + col + " on sheet " + sName);
            foreach (Sheet sheet in _doc.WorkbookPart.Workbook.GetFirstChild<Sheets>())
            {
                if (sheet.Name == sName)
                {
                    WorksheetPart worksheetPart = (WorksheetPart)_doc.WorkbookPart.GetPartById(sheet.Id);
                    WorksheetWriter ww = new WorksheetWriter(_doc, worksheetPart);
                    Column c = ww.FindColumn(col);
                    c.Hidden = true;
                }
            }
        }
    }
}