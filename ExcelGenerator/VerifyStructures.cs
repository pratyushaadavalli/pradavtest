﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable IDE1006 // Naming Styles
namespace ExcelGenerator
{

    public class VerMetadataStructure
    {
        public string name;
        public DateTime validisUploadDate;
        public DateTime workpaperDate;
        public string type;
        public DateTime startDate;
        public DateTime endDate;
        public Metrics metrics;
        public string callback;
        public string locale = "en_US";
        public bool largeDataSet;
        public string meVersion = "";
        public string v8Version = "";
    }

    public class Metrics
    {
        public float CJOutflowSum;
        public float CJInflowCount;
        public float CJOutflowCount;
        public float CJSum;
        public int CJCount;
        public int GLCount;
        public float CJDebitSum;
        public float RevenueSum;
        public float CJCreditSum;
        public float CJInflowSum;
        public int PostARCount;
        public int ARLinksCount;
        public int AccountCount;
        public int CJDebitCount;
        public float CJSuccessSum;
        public int InvoiceCount;
        public int PaymentCount;
        public int RevenueCount;
        public int CJCreditCount;
        public int CJTracedCount;
        public int CustomerCount;
        public int GLTracedCount;
        public int[] MultiCJGLTXID;
        public int ARGLLinksCount;
        public int ReceivableCount;
        public float TraceEndpointSum;
        public int CJReconciledCount;
        public float CJSuccessInflowSum;
        public int TraceEndpointCount;
        public int CJReconciledPercent;
        public int CJUnreconciledCount;
        public int CJTracedCountPercent;
        public int GLTracedCountPercent;
        public int RevenueEndpointCount;
        public float CJReconciledInflowSum;
        public int UndepositedFundsCount;
        public int RevenueEndpointPercent;
        public int CJReconciledInflowCount;
        public int DepositDetailMatchCount;
        public int UndepositedFundsAccountID;
        public float ReconciledRevenueEndpointSum;
        public int ReconciledRevenueEndpointCount;
    }

    public class VerBankItem
    {
        public string v8_type;
        public string verified;
        public Nullable<DateTime> date;
        public double amount;
        public double debitValue;
        public double creditValue;
        public string transactionType;
        public string checkNo;
        public string description;
        public string transactionId;
        public string accountType;
        public string accountCurrency;
        public string accountName;
        public string filename;
        public string page;
        public string matchIdBankErp;
    }

    public class VerGlItem
    {
        public string verified;
        public string v8Type;
        public string id;
        public string glTxId;
        public string baseType;
        public Nullable<DateTime> date;
        public double? amount;
        public double? debitAmount;
        public double? creditAmount;
        public string currency;
        public double? exchangeRate;
        public double? convertedAmount;
        public string[] bankTxnIds;
        public int? accountId;
        public string accountCode;
        public string accountName;
        public string accountCategoryTag;
        public string accountCategoryName;
        public string accountCategoryType;
        public int? sheetId;
        public string remotePk;
        public Nullable<DateTime> periodEnd;
        public string lineSource;
        public string sheetLabel;
        public string finYearName;
        public Nullable<DateTime> periodStart;
        public string headerSource;
        public bool? journalEntry;
        public string finPeriodName;
        public string lineDescription;
        public string headerDescription;
        public string txnType;
    }


    public class VerArItem
    {
        public string verified;
        public string v8Type;
        public string id;
        public string custSupTxId;
        public string baseType;
        public string date;
        public double? amount;
        public double? debitAmount;
        public double? creditAmount;
        public string currency;
        public double? exchangeRate;
        public double? convertedAmount;
        public string[] bankTxnIds;
        public string docId;
        public double? custSupId;
        public string custSupName;
        public string custSupDocId;
        public string txTypeName;
        public double? txTypeId;
        public double? taxValue;
        public DateTime? dueDate;
        public DateTime? periodStart;
        public DateTime? periodEnd;
        public string finYearName;
        public string finPeriodEnd;
        public DateTime? settledTimestamp;
        public string remotePk;
        public double? bankRec;
        public double? signValue;
        public string invTxId;
        public string invDate;
        public double? invAmount;
        public int? invDaysOutstanding;
        public bool? arApAudit;
        public float? arApBal;
        public float? subArApBal;
        public float? subAmount;
    }


    public class VerCompItem
    {
        public int? accountId;
        public string account;
        public string code;
        public string primaryCategory;
        public string tag;
        public double startingBal;
        public double activity;
        public double endingBalance;
    }


    public class VerTraceItem
    {
        public string verified;
        public int reconciled;
        public string traceId;
        public string glUUID;
        public string glTxId;
        public DateTime? date;
        public float amount;
        public float debitAmount;
        public float creditAmount;
        public float convertedAmount;
        public int accountId;
        public string accountName;
        public string accountCode;
        public string categoryName;
        public string categoryTag;
        public string sheet;
        public string source;
        public string baseType;
        public int errorCode;
        public string errorMessage;
        public string type;
        public VerTraceEndpoint[] endpoints;
    }

    public class VerTraceEndpoint
    {
        public string endpointGlUUID;
        public string endpointGlTxId;
        public DateTime? endpointDate;
        public float endpointAmount;
        public float endpointDebit;
        public float endpointCredit;
        public float endpointConvertedAmount;
        public float endpointAppliedAmount;
        public int endpointAccountId;
        public string endpointAccountCode;
        public string endpointAccountName;
        public string endpointCategoryTag;
        public string endpointCategoryName;
        public string endpointSheet;
        public string endpointSource;
        public string endpointBaseType;
    }

}
#pragma warning restore IDE1006 // Naming Styles
